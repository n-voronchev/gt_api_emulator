# Requirements

## Python
1. Pyhton 3.6 or newer (f-string feature)

2. Pip packages:
```
pip install -r requirements.txt
```

## AxxonNext proper version

Issues [ACR-56982](https://support.axxonsoft.com/jira/browse/ACR-56982), [ACR-56984](https://support.axxonsoft.com/jira/browse/ACR-56984), [ACR-56986](https://support.axxonsoft.com/jira/browse/ACR-56986) must be solved.

One may find patched [vmda_db.dll](https://support.axxonsoft.com/jira/secure/attachment/1335893/vmda_db.7z) library as [ACR-56982](https://support.axxonsoft.com/jira/browse/ACR-56982) attachment.

# Usage

## Make gRPC stubs

1. Unpack NGP's proto-files (see [protos/ReadMe.txt](protos/ReadMe.txt))

2. Run script:
```
python make_stubs.py
```

## Configure emulator

Edit `config.ini`

## Global Tracking API emulator server
```
python server.py
```

## Global Tracking API emulator client
```
python client.py
```

## Features

* The `gtGatherObjects()` method stores bounding boxes in an internal buffer of the "global tracker". Boundaries of the gathering time interval are the following:
    * a moment of a last call of `gtCreate()` ("global tracker" creation) or `gtGatherObjects()` (previous gathering) as the boundary in the past;
    * the present time as the second boundary.

* Don't assign multiple detectors to a camera. It isn't known in advance which detector will be used by API emulator to get bounding boxes.

* API emulator uses the first camera's vide stream (so called `SourceEndpoint.video:0:0`) to calculate bounding boxes.