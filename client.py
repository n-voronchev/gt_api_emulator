import arrow
import grpc
import logging
import os
import time
from contextlib import contextmanager

from stuff.config import read_config, loglevel_str_to_numeric
from stuff.grpc_helper import append_stubs_path

base_dir_path = os.path.realpath(os.path.join(__file__, '..'))
config = read_config(f'{base_dir_path}/config.ini')
append_stubs_path(config)

import axxonsoft.gt_rev4.gt_rev4a_pb2 as gt_rev4a_pb2
import axxonsoft.gt_rev4.gt_rev4a_pb2_grpc as gt_rev4a_pb2_grpc


class GtApiEmulatorCleint:

    def __init__(self, channel):
        self._stub = gt_rev4a_pb2_grpc.GTServiceStub(channel)

    def gt_create(self, cam_id) -> gt_rev4a_pb2.GlobalTracker:
        response = self._stub.gtCreate(
            gt_rev4a_pb2.gtCreateRequest(
                cams=[
                    gt_rev4a_pb2.GtCamera(
                        id=cam_id,
                        calibrationPoints=[]
                    )
                ]
            )
        )
        logging.info(f'>>> gtCreate() with cam_id={cam_id}:\n{response}')
        return response.tracker

    def gt_gather_objects(self, tracker):
        response = self._stub.gtGatherObjects(
            gt_rev4a_pb2.gtGatherObjectsRequest(
                tracker=tracker
            )
        )
        logging.info(f'>>> gt_gather_objects() with tracker.id={tracker.id}:\n{response}')

    def gt_get_objects_count(self, tracker):
        response = self._stub.gtGetObjectsCount(
            gt_rev4a_pb2.gtGetObjectsCountRequest(
                tracker=tracker
            )
        )
        logging.info(f'>>> gt_get_objects_count() with tracker.id={tracker.id}:\nresponse.count={response.count}\n')

    def gt_get_objects(self, tracker):
        response = self._stub.gtGetObjects(
            gt_rev4a_pb2.gtGetObjectsRequest(
                tracker=tracker
            )
        )

        s = str()
        for item in response:
            obj = item.object
            geo_pos = obj.position
            frame_pos = geo_pos.onFramePositions
            for p in frame_pos:
                local_time = arrow.get(p.time).to('local')
                s += f"cam_id={p.cameraId}: time-local={local_time} x={p.x:.2f} y={p.y:.2f} w={p.width:.2f} h={p.height:.2f} geoX={geo_pos.geoX:.2f} geoY={geo_pos.geoY:.1f} geoZ={geo_pos.geoZ:.1f}\n"

        logging.info(f'>>> gt_get_objects() with tracker.id={tracker.id}:\n{s}\n')

    def gt_destroy(self, tracker):
        response = self._stub.gtDestroy(
            gt_rev4a_pb2.gtDestroyRequest(
                tracker=tracker
            )
        )
        logging.info(f'>>> gt_destroy() with tracker.id={tracker.id}:\n{response}')


def run(channel):
    api_client = GtApiEmulatorCleint(channel)
    
    tracker = api_client.gt_create(config['api_client']['cam_id'])
    logging.info(f'>>> sleeping start at {arrow.now()}...')
    time.sleep(1) # 2*60
    logging.info(f'>>> sleeping finished at {arrow.now()}')
    api_client.gt_gather_objects(tracker)
    api_client.gt_get_objects_count(tracker)
    api_client.gt_get_objects(tracker)
    api_client.gt_destroy(tracker)


@contextmanager
def open_channel(config):
    channel = grpc.insecure_channel(config['api_emulator']['addr'] + ':' + config['api_emulator']['grpc_port'])
    try:
        yield channel
    finally:
        channel.close()


if __name__ == '__main__':
    logging.basicConfig(
        level=loglevel_str_to_numeric(config['api_client']['log_level']))
    with open_channel(config) as channel:
        run(channel)
