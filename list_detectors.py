import os
from tabulate import tabulate

from stuff.config import read_config, loglevel_str_to_numeric
from stuff.grpc_helper import append_stubs_path

base_dir_path = os.path.realpath(os.path.join(__file__, '..'))
config = read_config(f'{base_dir_path}/config.ini')
append_stubs_path(config)
from stuff.native_bl_helper import NgpHost

ngp_host = NgpHost(config)
cameras = ngp_host._list_cams()

table = list()
for cam in cameras:
    for det in cam.detectors:
        for sd in det.scene_descriptions:
            if not sd.access_point.endswith('vmda'):
                continue
            table.append([
                f'{cam.display_id}.{cam.display_name}',
                f'{det.display_id}.{det.display_name}',
                sd.access_point])
    
table_str = tabulate(
    table,
    tablefmt='presto',
    headers=['Camera name', 'Detector name', 'Scene Descriptions VMDA access point (AP)'])

print(f'{os.linesep}{table_str}{os.linesep}')
