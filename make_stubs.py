import glob
import shutil
from os import path, mkdir

import grpc_tools.protoc
protoc_py_path = grpc_tools.protoc.__file__
grpc_tools_proto_dir = path.realpath(path.join(protoc_py_path, '..', '_proto'))

base_dir = path.realpath(path.join(__file__, '..'))
protos_dir=path.join(base_dir, 'protos')
output_stubs_dir=path.join(base_dir, 'stubs')

shutil.rmtree(output_stubs_dir, onerror=lambda function, path, excinfo: print(f'WARN: {path}: {function}: {excinfo}'))
mkdir(output_stubs_dir)

proto_files = glob.glob(f'{protos_dir}/**/*.proto', recursive=True)
for pf in proto_files:
    print(f'INFO: Process {pf}')
    grpc_tools.protoc.main([
        protoc_py_path,
        f'-I{protos_dir}',
        f'--python_out={output_stubs_dir}',
        f'--grpc_python_out={output_stubs_dir}',
        f'{pf}',
        f'-I{grpc_tools_proto_dir}'])

