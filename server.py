import grpc
import logging
import os
from concurrent.futures import ThreadPoolExecutor

from stuff.config import read_config, loglevel_str_to_numeric
from stuff.grpc_helper import append_stubs_path

base_dir_path = os.path.realpath(os.path.join(__file__, '..'))
config = read_config(f'{base_dir_path}/config.ini')
append_stubs_path(config)

from stuff.gtservice import GTService


def serve():
    server = grpc.server(ThreadPoolExecutor(max_workers=1))
    service = GTService(config)
    GTService.add_service_to_server(service, server)

    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(
        level=loglevel_str_to_numeric(config['api_emulator']['log_level']))
    serve()
