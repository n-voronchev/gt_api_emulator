import configparser
import logging
import os


def read_config(config_path):
    config = configparser.ConfigParser(
        defaults={
            'root_dir': os.path.dirname(os.path.realpath(config_path))
        })
    config.read(config_path)
    return config


def path_in_rootdir(config, relative_path):
    return os.path.realpath(os.path.join(config['DEFAULT']['root_dir'], relative_path))


def loglevel_str_to_numeric(ll_str):
    """ see https://docs.python.org/3/howto/logging.html """
    ll_num = getattr(logging, ll_str.upper(), None)
    if not isinstance(ll_num, int):
        raise ValueError(f'Invalid log level: {ll_str}')
    return ll_num
