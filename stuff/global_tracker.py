from __future__ import annotations
from datetime import timedelta
import arrow
import ast
import uuid
from os import path
from typing import NamedTuple

import numpy as np

from stuff.config import path_in_rootdir
from stuff.native_bl_helper import NgpHost


class GlobalTracker():

    def __init__(self, cam_id, config):
        self._uuid = uuid.uuid1()
        self._cam_id = cam_id

        self._ngp_host = NgpHost(config)

        self._vmda_ap = config['api_emulator']['vmda_access_point']
        self._detector_id = self._ngp_host.get_first_detector_ap(cam_id)
        self._found_tracks = list()

        self._search_area = ast.literal_eval(config['api_emulator']['search_area'])

        self._begin_time = arrow.now() - timedelta(minutes=2)

        self._frame2geo_T = np.matrix(np.loadtxt(path_in_rootdir(config, f'geoT_cam{self._cam_id}.txt')))

    @property
    def uuid(self):
        return self._uuid

    @property
    def cam_id(self):
        return self._cam_id

    def vmda_search(self):
        end_time = arrow.now()

        intervals = self._ngp_host.vmda_execute_motion_in_areay_query(
            vmda_ap=self._vmda_ap,
            detector_id=self._detector_id,
            begin_time=self._begin_time,  #"past" not works
            end_time=end_time,  #"future" not works
            contour_vertices=self._search_area  # look for 'query' examples in the VMDA service logs.
        )

        self._found_tracks = [
            VmdaTrack(arrow.get(i.limit.begin_time), arrow.get(i.limit.end_time), o.id, o.left, o.top, o.right, o.bottom)
            for i in intervals for o in i.objects]

        self._begin_time = end_time
                
    @property
    def vmda_search_results_count(self):
        return len(self._found_tracks)

    @property
    def vmda_search_results(self) -> list[VmdaTrack]:
        """ Возвращает список из :class:`Track` """
        return self._found_tracks

    def convert_coord_frame2geo(self, vmda_track: VmdaTrack) -> GeoCoord:
        frame_w, frame_h = self._ngp_host.get_first_video_stream_resolition(self._cam_id)

        x_frame = frame_w * float(vmda_track.left + vmda_track.right) / 2
        y_frame = frame_h * float(vmda_track.top + vmda_track.bottom) / 2
        
        v_frame = np.matrix([x_frame, y_frame, 1]).transpose()
        v_geo = self._frame2geo_T * v_frame

        x_geo_t, y_geo_t, t = v_geo.getA1()
        return GeoCoord(x_geo_t / t, y_geo_t / t, 0)  # TODO: z - ???


class GeoCoord(NamedTuple):
    x: float
    y: float
    z: float


class VmdaTrack(NamedTuple):
    begin: arrow.arrow.Arrow
    end: arrow.arrow.Arrow
    id: int
    left: float
    top: float
    right: float
    bottom: float


def ts_as_local(utc_ts):
    return arrow.get(utc_ts).to('local').format("YYYY-MM-DD HH:mm:ss")
