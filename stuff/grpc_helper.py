import os
import sys


def append_stubs_path(config):
    stubs_path = os.path.join(config['DEFAULT']['root_dir'], 'stubs')
    sys.path.append(stubs_path)