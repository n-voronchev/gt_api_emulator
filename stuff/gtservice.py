from __future__ import annotations
import uuid

from stuff.global_tracker import GlobalTracker

import axxonsoft.gt_rev4.gt_rev4a_pb2 as gt_rev4a_pb2
import axxonsoft.gt_rev4.gt_rev4a_pb2_grpc as gt_rev4a_pb2_grpc

class GTService(gt_rev4a_pb2_grpc.GTServiceServicer):
    
    @classmethod
    def add_service_to_server(cls, service: GTService, server):
        gt_rev4a_pb2_grpc.add_GTServiceServicer_to_server(service, server)

    def __init__(self, config):
        self._config = config
        self.global_trackers: dict[uuid.UUID, GlobalTracker] = dict()

    def gtCreate(self, request, context):
        tracker = GlobalTracker(request.cams[0].id, self._config)
        self.global_trackers[tracker.uuid] = tracker
        return gt_rev4a_pb2.gtCreateResponse(
            tracker=gt_rev4a_pb2.GlobalTracker(id=str(tracker.uuid)))

    def gtGatherObjects(self, request, context):
        tracker_uuid = uuid.UUID(request.tracker.id)
        tracker = self.global_trackers[tracker_uuid]
        tracker.vmda_search()
        return gt_rev4a_pb2.gtGatherObjectsResponse()

    def gtGetObjectsCount(self, request, context):
        tracker_uuid = uuid.UUID(request.tracker.id)
        tracker = self.global_trackers[tracker_uuid]
        tracks_count = tracker.vmda_search_results_count
        return gt_rev4a_pb2.gtGetObjectsCountResponse(count=tracks_count)

    def gtGetObjects(self, request, context):
        tracker_uuid = uuid.UUID(request.tracker.id)
        tracker = self.global_trackers[tracker_uuid]
        tracks = tracker.vmda_search_results
        for track in tracks:
            geo_pos =  tracker.convert_coord_frame2geo(track)
            yield gt_rev4a_pb2.gtGetObjectsResponse(
                object=gt_rev4a_pb2.GlobalTrack(
                    type=0,
                    state=gt_rev4a_pb2.ObjectState.gtNormal,
                    position=gt_rev4a_pb2.GeoPosition(
                        geoX=geo_pos.x,
                        geoY=geo_pos.y,
                        geoZ=geo_pos.z,
                        time=0,
                        onFramePositions=[
                            gt_rev4a_pb2.OnFramePosition(
                                x=track.left,
                                y=track.top,
                                width=track.right - track.left,
                                height=track.bottom - track.top,
                                time=int(track.begin.timestamp()),  # unix timestamp
                                cameraId=tracker.cam_id
                            )
                        ]
                    ),
                    descriptors=[
                        gt_rev4a_pb2.ObjectDescriptor(
                            type=0,
                            subtype="",
                            version=0,
                            data=list()
                        )
                    ]
                )
            )

    def gtCorrectExistedTracks(self, request, context):
        return gt_rev4a_pb2.gtCorrectExistedTracksResponse()

    def gtGetCorrectedTracksCount(self, request, context):
        return gt_rev4a_pb2.gtGetCorrectedTracksCountResponse(count=0)

    def gtGetCorrectedTracks(self, request, context):
        return gt_rev4a_pb2.gtGetCorrectedTracksResponse(correctedTacks=list())

    def gtDestroy(self, request, context):
        tracker_uuid = uuid.UUID(request.tracker.id)
        self.global_trackers.pop(tracker_uuid)
        return gt_rev4a_pb2.gtDestroyResponse()

    def gtFreeGlobalTrack(self, request, context):
        return gt_rev4a_pb2.gtFreeGlobalTrackResponse()