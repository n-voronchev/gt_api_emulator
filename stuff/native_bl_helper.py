import arrow
import grpc
import logging
from itertools import chain

from stuff.config import path_in_rootdir

import axxonsoft.bl.auth.Authentication_pb2 as Authentication_pb2
import axxonsoft.bl.auth.Authentication_pb2_grpc as Authentication_pb2_grpc
import axxonsoft.bl.domain.Domain_pb2 as Domain_pb2
import axxonsoft.bl.domain.Domain_pb2_grpc as Domain_pb2_grpc
import axxonsoft.bl.statistics.Statistics_pb2 as Statistics_pb2
import axxonsoft.bl.statistics.Statistics_pb2_grpc as Statistics_pb2_grpc
import axxonsoft.bl.vmda.VMDA_pb2 as VMDA_pb2
import axxonsoft.bl.vmda.VMDA_pb2_grpc as VMDA_pb2_grpc


class NgpHost():

    def __init__(self, config):
        self._channel = _open_ngp_channel(config)

    def vmda_execute_motion_in_areay_query(self, vmda_ap, detector_id, begin_time, end_time, contour_vertices):
        # Virtual cameras workaround:
        # begin_time -= datetime.timedelta(seconds=60)
        # end_time += datetime.timedelta(seconds=60)

        request = VMDA_pb2.ExecuteQueryRequest(
            access_point=vmda_ap,
            camera_ID=detector_id,
            schema_ID="vmda_schema",
            dt_posix_start_time=_get_utc_ts(begin_time),  #"past" not works
            dt_posix_end_time=_get_utc_ts(end_time),  #"future" not works
            query=_make_motion_in_area_query(*contour_vertices),
            language="EVENT_BASIC")

        stub = VMDA_pb2_grpc.VMDAServiceStub(self._channel)
        response = stub.ExecuteQuery(request)

        intervals = list(chain.from_iterable(chunk.intervals for chunk in response))
        logging.debug(f'NgpHost.vmda_execute_motion_in_areay_query: detector_id={detector_id} begin_time={begin_time} end_time={end_time} -> {len(intervals)}')
        return intervals

    def get_first_video_stream_resolition(self, cam_id):
        cam = self._get_cam(cam_id)
        stream_ap = cam.video_streams[0].stream_acess_point

        request = Statistics_pb2.StatsRequest(
            keys=[
                Statistics_pb2.StatPointKey(
                    type=Statistics_pb2.SPT_LiveWidth,
                    name=stream_ap
                ),
                Statistics_pb2.StatPointKey(
                    type=Statistics_pb2.SPT_LiveHeight,
                    name=stream_ap
                )
            ]
        )

        stub = Statistics_pb2_grpc.StatisticServiceStub(self._channel)
        response = stub.GetStatistics(request)

        stats = {s.key.type: s.value_uint32 for s in response.stats}
        w, h = stats[Statistics_pb2.SPT_LiveWidth], stats[Statistics_pb2.SPT_LiveHeight]
        logging.debug(f'NgpHost.get_first_video_stream_resolition: cam_id={cam_id} -> {(w, h)}')
        return (w, h)

    def get_first_detector_ap(self, cam_id):
        cam = self._get_cam(cam_id)        

        for det in cam.detectors:
            for sd in det.scene_descriptions:
                if not sd.access_point.endswith('vmda'):
                    continue
                _, _, det_name, ep_name = sd.access_point.split('/')
                det_short_ap = f'{det_name}/{ep_name}'
                logging.debug(f'NgpHost.get_first_detector_ap: cam_id={cam_id} -> {det_short_ap}')
                return det_short_ap
                    
        raise Exception(f'Cann\'t find any suitable detector for camera with id={cam_id}.')

    def _get_cam(self, cam_id):
        cams = [c for c in self._list_cams() if str(c.display_id) == str(cam_id)]
        if len(cams) > 1:
            raise Exception(repr(cams))
        if len(cams) == 0:
            raise Exception(f'Camera with id={cma_id} not found.')
        return cams[0]

    def _list_cams(self):
        stub = Domain_pb2_grpc.DomainServiceStub(self._channel)
        request = Domain_pb2.ListCamerasRequest(view=Domain_pb2.EViewMode.VIEW_MODE_FULL)  # filter=None, group_ids=None, 
        response = stub.ListCameras(request)
        cameras = list(chain.from_iterable(ri.items for ri in response))        
        return cameras


def _get_utc_ts(t: arrow.arrow.Arrow):
    return t.to('utc').format("YYYYMMDDTHHmmss.001")


def _make_motion_in_area_query(*contour_vertices):
    """
    contour_vertices:
        - each vertex is (x,y) point
        - clockwise direction
        - top-left corner is the origin of coordinate system -- (0,0) point
    """
    polygon = ",".join(str(float(x)) for x in chain.from_iterable(contour_vertices))
    return f'figure fZone=polygon({polygon}); figure fDir=(ellipses(-10000, -10000, 10000, 10000) - ellipses(-0, -0, 0, 0));set r = group[obj=vmda_object] {{ res = or(fZone((obj.left + obj.right) / 2, obj.bottom)) }}; result = r.res;'


def _open_ngp_channel(config):
    grpc_host = config['ngp_host']['addr'] + ':' + config['ngp_host']['grpc_port']
    login = config['ngp_host']['login']
    password = config['ngp_host']['password']
    certificate_path = path_in_rootdir(config, config['common']['certificate_path'])

    with open(certificate_path, 'rb') as f:
        certificate = f.read()
    channel_credentials = grpc.ssl_channel_credentials(root_certificates=certificate)
    ssl_channel = grpc.secure_channel(
        grpc_host,
        channel_credentials,
        options=(('grpc.ssl_target_name_override', "Anonymous",),))

    try:
        auth_client = Authentication_pb2_grpc.AuthenticationServiceStub(ssl_channel)
        auth_request = Authentication_pb2.AuthenticateRequest(user_name=login, password=password)
        auth_response = auth_client.Authenticate(auth_request)
    finally:
        ssl_channel.close()

    auth_header = (auth_response.token_name, auth_response.token_value)
    call_credentials = grpc.metadata_call_credentials(lambda _, cb: cb([auth_header], None))
    composite_channel_credentials = grpc.composite_channel_credentials(channel_credentials, call_credentials)
    channel = grpc.secure_channel(
            grpc_host,
            composite_channel_credentials,
            options=(('grpc.ssl_target_name_override', "Anonymous",),)
        )
    
    return channel
    